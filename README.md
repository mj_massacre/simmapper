## SimMapper 
<img src="https://gitlab.com/mj_massacre/simmapper/-/jobs/1369670855/artifacts/raw/public/view/img/logo_transparent.png" height=90>


A tool using DruidJS, which is a JavaScript library for dimensionality reduction. 
With dimesionality reduction one can project high-dimensional data to a lower dimensionality while keeping method-specific properties of the data.
DruidJS makes it easy to project a dataset with the implemented dimensionality reduction methods.

SimMapper lets users open CSV datasets and creates visualizations with Plotly, which can
be customized in their shape, color and sizes. This application also allows the export of
these visualizations as SVG. 

## Building and Running

The application requires `npm` to build and run

* `npm install`

   In the root directory will install all required dependencies. The project uses `gulp` tasks to perform all steps required to build and execute the final application. The following tasks are available (you can always run `gulp --tasks` to see an overview of all avialable tasks):

* `npx gulp`

   This task builds the application from scratch. If an old build exists, it is removed before the new build is started. After the build is finished, the application is
   started automatically. Since this is the default task, it is also started if one simply runs `gulp` without a task name.

* `npx gulp clean`

    Removes any existing build artifacts.

* `npx gulp build`

    Removes existing artifacts and builds the application, but does not run it.

* `npx gulp run`

    Runs the application. The application must have been built using `npx gulp build` before it can be run this way.

* `npx gulp app`

    Creates an executable from the application that can be run standalone. The application must have been built using `npx gulp build` before it can be exported.

* `npx rollup -c`

    Creates a minified bundle from the application. 




This project has been developed during the course 'Information Visualization' in 
the summer term 2021 at Graz, University of Technology.

The authors are Jihad Itani, Piotr Kupiec, Emanuel Moser and Martin Sackl.