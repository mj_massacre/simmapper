// import merge from 'deepmerge';
// import { createSpaConfig } from '@open-wc/building-rollup';
// import typescript from '@rollup/plugin-typescript';

// const baseConfig = createSpaConfig();

// export default merge(baseConfig, {
//   input: './index.html',
//   plugins: [typescript()],
// });

export default [
    {
      input: 'dist/main.js',
      output: {
        file: __dirname + '/public/main.js',
        format: 'cjs',
        name: 'main'
      }
    },
    {
      input: 'dist/data.js',
      output: {
        file: __dirname + '/public/data.js',
        format: 'cjs',
        name: 'data'
      }
    }
  ]
  